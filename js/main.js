/*! Main */
jQuery(document).ready(function ($) {


    var createView = function(data){
        if(data.Poster ==="N/A"){
            data.Poster = "img/poster.jpg";
        }
        var html = '<div class="col-md-4 movie-results-block"> <div class="movie-title"> <h2 class="movie-title-h2 Title">'+data.Title+'</h2> </div> <div class="movie-box"> <div class="row movie-thumb"> <img src="'+data.Poster+'" class="img-responsive"> </div><div class="row movie-rate"> <fieldset class="rating"> <input type="radio" id="star5" name="rating" value="5" /> <label class="full" for="star5" title="Awesome - 5 stars"></label> <input type="radio" id="star4half" name="rating" value="4 and a half" /> <label class="half" for="star4half" title="Pretty good - 4.5 stars"></label> <input type="radio" id="star4" name="rating" value="4" /> <label class="full" for="star4" title="Pretty good - 4 stars"></label> <input type="radio" id="star3half" name="rating" value="3 and a half" /> <label class="half" for="star3half" title="Meh - 3.5 stars"></label> <input type="radio" id="star3" name="rating" value="3" /> <label class="full" for="star3" title="Meh - 3 stars"></label> <input type="radio" id="star2half" name="rating" value="2 and a half" /> <label class="half" for="star2half" title="Kinda bad - 2.5 stars"></label> <input type="radio" id="star2" name="rating" value="2" /> <label class="full" for="star2" title="Kinda bad - 2 stars"></label> <input type="radio" id="star1half" name="rating" value="1 and a half" /> <label class="half" for="star1half" title="Meh - 1.5 stars"></label> <input type="radio" id="star1" name="rating" value="1" /> <label class="full" for="star1" title="Sucks big time - 1 star"></label> <input type="radio" id="starhalf" name="rating" value="half" /> <label class="half" for="starhalf" title="Sucks big time - 0.5 stars"></label> </fieldset></div><div class="row movie-description"> <p class="year"> <label for="year">Year:</label> <span class="year-1 Year">'+data.Year+'</span> </p> <p class="genre"> <label for="genre">Genre:</label> <span class="genre-1">'+data.Genre+'</span> </p> <p class="year"> <label for="director">Director:</label> <span class="director-1">'+data.director+'</span> </p> <p class="actors"> <label for="actors">Actors:</label> <span class="actors-1">'+data.Actors+', </span> </p> <p class="plot"> <label for="plot">Plot:</label> <span class="plot-1">'+data.Plot+'</span></p></div></div></div>';
        $("#movielList").append(html);
    };

    //Fetching poster, rating, genre, etc
    var fetchCompleteDetails = function(data){
        data.forEach(function(value){
            var url = "http://www.omdbapi.com/?i="+value.imdbID;
            $.getJSON(url, function( data, status, xhr ) {
                if(data.Response === "True"){
                    createView(data);
                }
            });
        });
    };
    
    //Autcomplete
    $("#searchAutoComplete").autocomplete({
        minLength: 2,
        source: function( request, response ) {
            $("#movielList").empty();
            var term = request.term;
            var genre = $("#genre").val();
            var url = "http://www.omdbapi.com/?s="+term;
            $.getJSON(url, function( data, status, xhr ) {
                if(data.Response === "True"){
                    var result = [];
                    data.Search.forEach(function(value){
                        result.push(value.Title);
                    });
                    response(result);
                }
            });
        }
    });

    //Search
    $("#searchAutoComplete").keypress(function (e) {
        $("#movielList").empty();
        if (e.which == 13) {
            var term = $("#searchAutoComplete").val();
            var url = "http://www.omdbapi.com/?s="+term;
            $.getJSON(url, function(data, status, xhr ) {
                if(data.Response === "True"){
                    fetchCompleteDetails(data.Search);
                };
            });
            return false;
        }
    });

    
});
